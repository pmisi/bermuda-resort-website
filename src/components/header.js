import { Link } from "gatsby";
import PropTypes from "prop-types";
import React from "react";

import "../css/header.css";

const Header = () => (
  <header className="header">
    <div className="container navbar">
      <h1 id="branding">
        <Link to="/" className="link">
          Bermuda <span className="dark">Resort</span>
        </Link>
      </h1>
      <nav>
        <ul id="links">
          <li>
            <Link to="/szolgaltatasok" className="link">
              Szolgáltatások
            </Link>
          </li>
          <li>
            <Link to="/galeria" className="link">
              Galéria
            </Link>
          </li>
        </ul>
      </nav>
    </div>
  </header>
);

Header.propTypes = {
  siteTitle: PropTypes.string
};

Header.defaultProps = {
  siteTitle: ``
};

export default Header;
