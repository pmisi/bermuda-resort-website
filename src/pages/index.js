import React from "react";
import { Link } from "gatsby";

import Layout from "../components/layout";
import SEO from "../components/seo";

import "../css/index.css";

const IndexPage = () => (
  <Layout>
    <SEO title="Főoldal" keywords={[`gatsby`, `application`, `react`]} />
    <header className="showcase">
      <h1>Vesszen el a lehetőségek tengerében!</h1>
      <Link to="/szolgaltatasok" className="button dark">
        Szobák & Szolgáltatások
      </Link>
    </header>
    <section className="container">
      <h1>Szobák & ajánlatok</h1>
      <hr />
      <div className="room-grid">
        <img src={require("../images/rooms/budget/budget-1.jpeg")} />
        <div>
          <h2>Budget</h2>
          <p>
            Fedezze fel a városunkat és pihenjen meg a pénztárcabarát Budget
            szobáink egyikében. Kerékpár és vitorláshajó bérlése ingyenes, így e
            szobánk ideális választás egy hosszú hétvégés pihenésre. Sportolja
            ki magát a fitnesstermünkben és lazuljon el a kellemes
            termálfürdőnkben.
          </p>
        </div>
      </div>
      <div className="room-grid rtl">
        <img
          src={require("../images/rooms/mid/mid-1.jpeg")}
          className="item-1"
        />
        <div className="item-2">
          <h2>Mid</h2>
          <p>
            Amennyiben hosszabb időre szeretne kikapcsolódni, érdemes a Mid
            apartmanban megszállni. Az inkluzív teljes fitness és wellness
            belépő mellett ingyenesen kölcsönözhet biciklit és vitorláshajót.
            Vigye magával a kutyaszerű házi kedvencét is, mert az állatokat
            különleges bánásmódban részesítjük. Prémium eledellel, kiváló
            alvóhelyiséggel és elit kutyaiskolával ő is részesülhet a családi
            nyaralásban.
          </p>
        </div>
      </div>
      <div className="room-grid">
        <img src={require("../images/rooms/lux/lux-1.jpg")} />
        <div>
          <h2>Lux</h2>
          <p>
            A Lux apartmannal egyetemben teljes fitness és wellness
            szolgáltatásainkat ingyen igénybe veheti. A golfpályánk és
            szerszámaink ár nélkül, bármikor igénybe vehetőek. A 75
            négyzetméteres, kétszintes apartmanhoz saját billiárd asztal áll
            rendelkezésre. A háziállatokat kérdés nélkül részesítjük
            szolgáltatásainkban.
          </p>
        </div>
      </div>
    </section>
    <section id="feature_lobby">
      <h1>Nézzen körbe, mit nyújtunk Önnek!</h1>
      <Link to="/szolgaltatasok" className="button white">
        Szolgáltatásaink
      </Link>
    </section>
  </Layout>
);

export default IndexPage;
