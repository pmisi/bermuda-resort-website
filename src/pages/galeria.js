import React from "react";
import { Link } from "gatsby";

import Layout from "../components/layout";
import SEO from "../components/seo";
import "../css/galeria.css";

const SecondPage = () => (
  <Layout>
    <SEO title="Galéria" />
    <header className="gallery-showcase">
      <h1>Galéria</h1>
    </header>
    <section className="container">
      <div className="grid-3">
        <div>
          <h2>Budget szoba</h2>
          <img src={require("../images/rooms/budget/budget-1.jpeg")} alt="" />
          <img src={require("../images/rooms/budget/budget-2.jpeg")} alt="" />
          <img src={require("../images/rooms/budget/budget-3.jpeg")} alt="" />
        </div>
        <div>
          <h2>Mid szoba</h2>
          <img src={require("../images/rooms/mid/mid-1.jpeg")} alt="" />
        </div>
        <div>
          <h2>Lux apartman</h2>
          <img src={require("../images/rooms/lux/lux-1.jpg")} alt="" />
          <img src={require("../images/rooms/lux/lux-2.jpg")} alt="" />
        </div>
      </div>
    </section>
    <div className="team">
      <h1>A csapat</h1>
    </div>
  </Layout>
);

export default SecondPage;
