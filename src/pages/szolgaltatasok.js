import React from "react";
import { Link } from "gatsby";

import Layout from "../components/layout";
import SEO from "../components/seo";
import "../css/szolgaltatasok.css";

const SecondPage = () => (
  <Layout>
    <SEO title="Szobák & Szolgáltatások" />
    <header className="services-showcase">
      <h1>Szobák & Szolgáltatások</h1>
    </header>
    <h1>Wellness & Fitness</h1>
    <section className="container grid-2">
      <div>
        <img src={require("../images/wellness.jpeg")} alt="" />
        <h2>Wellness</h2>
        <p>
          Legkülönfélébb szaunák, medencék, sókamra, törökfürdő várja az
          ellazulni vágyókat. Szakképzett masszőrök gondoskodnak a pihenésről és
          az egészségről.
        </p>
      </div>

      <div>
        <img src={require("../images/fitness.jpeg")} alt="" />
        <h2>Fitness</h2>
        <p>
          Jól felszerelt fitnesstermünket nem csak a szálloda látogatói vehetik
          igénybe.
        </p>
      </div>
    </section>

    <h1>Gasztronómia</h1>
    <section className="container grid-2">
      <div>
        <img src={require("../images/restaurant.jpeg")} alt="" />
        <h2>Étterem</h2>
        <p>
          Hihetetlen választékú menünkben csak a szakácsain legjavát szolgáljuk
          ki. Nálunk mindig megtalálja a helyi és nemzetközi konyha legjobb
          fogásait.
        </p>
      </div>

      <div>
        <img src={require("../images/coffee.jpeg")} alt="" />
        <h2>Kávézó</h2>
        <p>
          Kellemes hangulatot biztosítunk, ha beszélgetni szeretne egy
          barátjával egy kávé társaságában. Ingyenes és gyors
          internetkapcsolatunkkal akár távolról dolgozhat is.
        </p>
      </div>
    </section>

 
  </Layout>
);

export default SecondPage;
